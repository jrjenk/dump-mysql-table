<?php
/**
 * A quick and dirty PHP file to dump MySQL Data.
 *
 * This script will connect to the specified MySQL database and attempt to
 * retrieve all columns in the specified table.  It will then save the data in
 * those columns, one column per line, to a text file in the DumpTable-Output
 * directory, where the name of the file can be based either on the value of a
 * user specified column, or will default to "EmptyColumn-##.##" where ##.##
 * is the number of seconds since 0:00:00 January 1,1970 GMT.  Both file names
 * will have a .txt extension.
 *
 * All column data is expected to be text based, and this script does not
 * support looking up data from other tables in the database or decoding data
 * in anyway.
 *
 * Don't judge me by this code, it was a quick hack and I wanted it to be simple
 * enough for someone else to edit quickly.
 *
 * PHP version 5.2
 *
 *
 * @author     Original Author <author@example.com>
 * @license    http://creativecommons.org/licenses/by-sa/4.0/
 * @version    1.0
 */


// The Hostname or IP Address of the MySQL Server
$dbServer = "127.0.0.1";

// The username to use to connect to MySQL, does not need to be root.
$dbUser = "root";

// The password to use to connect to MySQL
$dbPass = "123qwe";

// The database that contains the table
$dbName = "coa";

// The table to dump all columns for
$dbTable = "users";

// This is the column name that contains what you want to use as the file name.
// If for some reason this column is Empty then the Filename will be
// "EmptyColumn-##.##" where ##.## is the number of seconds since
// 0:00:00 January 1,1970 GMT.
// Note: If this column does not contain unique data you will replace results.
$ColumnName = "name";



/**************************************************
 * You should not need to change anything below here.
 **************************************************/


// Call our custom function to do the work
DumpTableOld($dbServer, $dbUser, $dbPass, $dbName, $dbTable, $ColumnName);

/**
 * @param string $dbServer
 * @param string $dbUser
 * @param string $dbPass
 * @param string $dbName
 * @param string $dbTable
 * @param string $ColumnName
 */
function DumpTableOld($dbServer, $dbUser, $dbPass, $dbName, $dbTable, $ColumnName = null) {
  $conn = mysql_connect($dbServer, $dbUser, $dbPass);
  mysql_select_db($dbName);

  if($output = mysql_query("select * from $dbTable", $conn)) {
    // Create an output directory in the current working directory if needed
    if (!file_exists("./DumpTable-Output")) {
      mkdir("./DumpTable-Output", 0755);
    }

    while ($result = mysql_fetch_assoc($output)):
      // Create the output file, checking for an empty column name just in case.
      if (empty($ColumnName) || empty($result[$ColumnName])) {
        $outputFile = "./DumpTable-Output/EmptyColumn-" . microtime(TRUE) . ".txt";
      }
      else {
        $outputFile = "./DumpTable-Output/" . $result[$ColumnName] . ".txt";
      }

      // Open a File for writing, this will overwrite
      $targetFile = fopen($outputFile, "w");
      foreach ($result as $key => $value) {
        fwrite($targetFile, "$key contains $value\n");
      }
    endwhile;
  }

}


